﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Vereinsverwaltung.Models
{
    public class Role
    {
        public int RoleID { get; set; }
        public string Name { get; set; }

    }
}