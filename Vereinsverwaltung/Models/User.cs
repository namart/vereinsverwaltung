﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Vereinsverwaltung.Models
{
    public class User
    {
        public int UserID { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie ein Passwort an")]
        public string Password { get; set; }
        public string PasswordSalt { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string LastName { get; set; } 

        [Required(ErrorMessage = "Bitte geben Sie eine Email an")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Bitte geben sie eine email an")]
        public string Email { get; set; }
        public DateTime CreationDate { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie ihren Geburtstag an")]
        [DisplayName("Geburtstag")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine Rolle an")]
        public int RoleID { get; set; }
        public virtual Role Role { get; set; }

        [Required]
        public int ClubID { get; set; }
        public virtual Club Club { get; set; }

        public virtual ICollection<Group> Groups { get; set; }

        public virtual ICollection<GroupLeaderAssignment> GroupLeaderAssignments { get; set; }

    }
}