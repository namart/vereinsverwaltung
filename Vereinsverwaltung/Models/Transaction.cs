﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vereinsverwaltung.Models
{
    public class Transaction
    {
        public int TransactionID { get; set; }

        public int CheckoutID { get; set; }
        public virtual Checkout Checkout { get; set; }

        public int UserID { get; set; }
        public virtual User User { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Betrag an")]
        public Double Amount { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie ein Transaktionsdatum an")]
        public DateTime PayDate { get; set; }
        public virtual ICollection<Checkout> Collections { get; set; }
        public string test { get; set; }
    }
}