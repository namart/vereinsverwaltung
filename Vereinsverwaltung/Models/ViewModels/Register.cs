﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel;

namespace Vereinsverwaltung.Models
{
    public class Register
    {
        [Required(ErrorMessage = "Bitte geben Sie einen Vereinsnamen an")]
        public String Vereinsnamen { set; get; }

        [Required(ErrorMessage = "Bitte geben Sie eine Email an")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage="Bitte geben sie eine email an")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Bitte geben Sie eine Passwort ein")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Bitte geben Sie das selbe Passwort nochmals ein")]
        [Compare("Password")]
        public String ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string LastName { get; set; }

        [Required]
        [DisplayName("Geburtstag")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BirthDate { get; set; }
    }
}