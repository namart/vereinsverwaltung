﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Vereinsverwaltung.Models.ViewModels
{
    public class UserViewModel
    {

        [Required(ErrorMessage = "Bitte geben Sie ein Passwort an")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine Email an")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Bitte geben sie eine email an")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Geburtstag")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine Rolle an")]
        public int RoleID { get; set; }


        public int GroupID { get; set; }

    }
}