﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vereinsverwaltung.Models
{
    public class Login
    {
        [Required]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Bitte geben sie eine email an")]
        public String Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }
    }
}