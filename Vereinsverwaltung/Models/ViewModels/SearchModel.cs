﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Vereinsverwaltung.Models.ViewModels
{
    [Serializable]
    public class SearchModel
    {
        public int ClubId { get; set; }
        public int Group { get; set; }
        public String Payment { get; set; }
        [DefaultValue("")]
        public String FirstName { get; set; }
        [DefaultValue("")]
        public String LastName { get; set; }
        [DefaultValue("")]
        public String Text { get; set; }
    }
}