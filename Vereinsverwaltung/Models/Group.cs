﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vereinsverwaltung.Models
{
    public class Group
    {
        public int GroupID { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Gruppennamen an")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine kurze Beschreibung an")]
        public string Description { get; set; }

        [Required]
        public int ClubID { get; set; }
        public virtual Group Club { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public virtual ICollection<GroupLeaderAssignment> GroupLeaderAssignments { get; set; }

        public virtual ICollection<Checkout> Checkouts { get; set; }

    }
}