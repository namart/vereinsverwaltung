﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vereinsverwaltung.Models
{
    public class GroupLeaderAssignment
    {
        public int GroupLeaderAssignmentID { get; set; }

        [Required]
        public int GroupID { get; set; }
        public virtual Group Group { get; set; }

        [Required]
        public int UserID { get; set; }
        public virtual User User { get; set; }
    }
}