﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Vereinsverwaltung.Models
{
    public class Club
    {
        public int ClubID { get; set; }

        [Required(ErrorMessage="Bitte geben Sie einen Vereinsnamen an")]
        [StringLength(20, ErrorMessage="Der Name ist zu lang")]
        public String Name { get; set; }
        public DateTime CreationDate { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}