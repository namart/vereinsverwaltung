﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Vereinsverwaltung.Models
{
    public class Checkout
    {
        public int CheckoutID { get; set; }

        public int ClubID { get; set; }

        public int GroupID { get; set; }
        public virtual Group Group { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Betrag an")]
        [DisplayName("Betrag")]
        public Double Amount { get; set; }

        [Required]
        [DisplayName("Fälligkeitsdatum")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime MaturityDate { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}