﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using Vereinsverwaltung.Models;
using Vereinsverwaltung.DAL;

namespace Vereinsverwaltung
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Club", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "GroupAddUsers", // Route name
                "Group/Edit/{groupid}/AddUser/{userid}", // URL with parameters
                new { controller = "Group", action = "AddUser", groupid = "", userid = "" } // Parameter defaults
            );

            routes.MapRoute(
                "GroupRemoveUsers", // Route name
                "Group/Edit/{groupid}/RemoveUser/{userid}", // URL with parameters
                new { controller = "Group", action = "RemoveUser", groupid = "", userid = "" } // Parameter defaults
            );

            routes.MapRoute(
                "GroupAddLeaders", // Route name
                "Group/Edit/{groupid}/AddLeader/{userid}", // URL with parameters
                new { controller = "Group", action = "AddLeader", groupid = "", userid = "" } // Parameter defaults
            );

            routes.MapRoute(
                "GroupRemoveLeaders", // Route name
                "Group/Edit/{groupid}/RemoveLeader/{userid}", // URL with parameters
                new { controller = "Group", action = "RemoveLeader", groupid = "", userid = "" } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            Database.SetInitializer<ClubContext>(new ClubInitializer());

            AreaRegistration.RegisterAllAreas();

            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}