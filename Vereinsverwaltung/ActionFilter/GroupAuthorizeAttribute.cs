﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vereinsverwaltung.Repositories;
using Vereinsverwaltung.Models;

namespace Vereinsverwaltung.ActionFilter
{
    public class GroupAuthorizeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            String userName = filterContext.HttpContext.User.Identity.Name;
            int groupid = (int)filterContext.ActionParameters["id"];

            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                IUserRepository userRepository = new UserRepository();
                IGroupRepository groupRepository = new GroupRepository();

                User loggedInUser = userRepository.GetUsertByEmail(userName);

                Group group = groupRepository.GetGroupByID(groupid);

                if (loggedInUser.ClubID != group.ClubID)
                {
                    filterContext.HttpContext.Response.Redirect("/Account/AccessViolation", true);
                }
            }
        }
    }
}