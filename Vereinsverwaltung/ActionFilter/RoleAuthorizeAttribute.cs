﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Vereinsverwaltung.Repositories;
using Vereinsverwaltung.Models;

namespace Vereinsverwaltung.ActionFilter
{
    public class RoleAuthorizeAttribute : ActionFilterAttribute
    {
        public String[] Role { get; set; }
        public String Redirect { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            String userName = filterContext.HttpContext.User.Identity.Name;

            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                IUserRepository userRepository = new UserRepository();
                User user = userRepository.GetUsertByEmail(userName);

                if (Role != null && Role.Count() > 0)
                {
                    bool userAuthorized = false;
                    foreach (string role in Role)
                    {
                        if (user != null && (role == "Gruppenleiter" && user.GroupLeaderAssignments.Count() > 0))
                        {
                            filterContext.Controller.ViewData["RoleId"] = user.RoleID;
                            filterContext.Controller.ViewData["Role"] = "Gruppenleiter";
                            userAuthorized = true;
                        }
                        else if (user != null && (user.Role.Name == role))
                        {
                            filterContext.Controller.ViewData["RoleId"] = user.RoleID;
                            userAuthorized = true;
                        }
                        else if ((string)filterContext.Controller.ViewData["Allowed"] == "yes")
                        {
                            filterContext.Controller.ViewData["RoleId"] = user.RoleID;
                            userAuthorized = true;
                        }
      
                    }
                    if (userAuthorized == false)
                    {
                        filterContext.Result = new RedirectResult("/Account/AccessViolation");
                    }
                }
                else
                {
                    if (user != null)
                    {
                        filterContext.Controller.ViewData["ClubId"] = user.ClubID;
                        filterContext.Controller.ViewData["UserId"] = user.UserID;
                        filterContext.Controller.ViewData["RoleId"] = user.RoleID;
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult("/Account/AccessViolation");
                    }
                }
            }
            else
            {
                if (Redirect != "")
                {
                    filterContext.Result = new RedirectResult("~/" + Redirect);
                }
                else
                {
                    filterContext.Result = new RedirectResult(FormsAuthentication.LoginUrl);
                }
            }   
        }
    }
}