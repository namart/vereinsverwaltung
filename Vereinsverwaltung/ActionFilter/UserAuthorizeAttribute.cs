﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vereinsverwaltung.Repositories;
using Vereinsverwaltung.Models;

namespace Vereinsverwaltung.ActionFilter
{
    public class UserAuthorizeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            String userName = filterContext.HttpContext.User.Identity.Name;
            if (filterContext.ActionParameters["id"] == null)
            {
                return;
            }
            int userid = (int)filterContext.ActionParameters["id"];

            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                IUserRepository userRepository = new UserRepository();
                IGroupRepository groupRepository = new GroupRepository();

                User loggedInUser = userRepository.GetUsertByEmail(userName);

                User user = userRepository.GetUsertByID(userid);

                if (loggedInUser.ClubID != user.ClubID)
                {
                    filterContext.HttpContext.Response.Redirect("/Account/AccessViolation", true);
                }

                //Check if loggedIn user is authorized Groupleader
                if (loggedInUser.GroupLeaderAssignments.Count() > 0)
                {
                    foreach (Group group in user.Groups)
                    {
                        foreach (GroupLeaderAssignment leaderGroup in loggedInUser.GroupLeaderAssignments)
                        {
                            if (group.GroupID == leaderGroup.GroupID)
                            {
                                filterContext.Controller.ViewData["Allowed"] = "yes";
                                break;
                            }
                        }
                    }
                }

                //Check if id is userid of loggedIn user
                if (loggedInUser.UserID == user.UserID)
                {
                    filterContext.Controller.ViewData["Allowed"] = "yes";
                }
            }

        }
    }
}