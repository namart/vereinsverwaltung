﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Vereinsverwaltung.Repositories;
using Vereinsverwaltung.Models;

namespace Vereinsverwaltung.ActionFilter
{
    public class GlobalAuthorizeAttribute : ActionFilterAttribute
    {
        private IUserRepository userRepository = new UserRepository();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            String userName = filterContext.HttpContext.User.Identity.Name;

            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {

                User user = this.userRepository.GetUsertByEmail(userName);

                if (user != null && (user.GroupLeaderAssignments.Count() > 0))
                {
                    filterContext.Controller.ViewData["GroupLeader"] = "yes";

                }
                else
                {
                    filterContext.Controller.ViewData["GroupLeader"] = "no";
                }

                if (user != null && (user.RoleID == 1))
                {
                    filterContext.Controller.ViewData["Role"] = 1;
                }
                else
                {
                    filterContext.Controller.ViewData["Role"] = 0;
                }
                if (user != null)
                {
                    filterContext.Controller.ViewData["ClubId"] = user.ClubID;
                    filterContext.Controller.ViewData["UserId"] = user.UserID;
                } 
            }
        }
    }
}