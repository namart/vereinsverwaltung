﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vereinsverwaltung.Repositories;
using Vereinsverwaltung.Models;

namespace Vereinsverwaltung.ActionFilter
{
    public class ClubAuthorizeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            String userName = filterContext.HttpContext.User.Identity.Name;

            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                IUserRepository userRepository = new UserRepository();
                User user = userRepository.GetUsertByEmail(userName);
                Club club = (Club) filterContext.ActionParameters["club"];
                if (user.ClubID != club.ClubID)
                {
                    filterContext.HttpContext.Response.Redirect("/Account/AccessViolation", true);
                }
            }
        }
    }
}