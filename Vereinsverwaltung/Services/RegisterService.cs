﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vereinsverwaltung.Models;
using System.Security.Cryptography;
using System.Web.Security;

namespace Vereinsverwaltung.Services
{
    public class RegisterService
    {
        private ClubContext db = new ClubContext();

        public Club CreateClub(String vereinsnamen)
        {
            Club club = new Club();
            club.Name = vereinsnamen;
            club.CreationDate = System.DateTime.Now;
            db.Clubs.Add(club);
            db.SaveChanges();
            return club;
        }

        public User CreateUser(User user)
        {
            user.PasswordSalt = CreateSalt();
            user.Password = CreatePasswordHash(user.Password, user.PasswordSalt);
            user.CreationDate = DateTime.Now;
            db.Users.Add(user);
            db.SaveChanges();

            return user;
        }

        public User CreateClubUser(Register register, int roleID)
        {
            Club club = CreateClub(register.Vereinsnamen);
            User user = new User();
            user.Email = register.Email;
            user.PasswordSalt = CreateSalt();
            user.Password = CreatePasswordHash(register.Password, user.PasswordSalt);
            user.LastName = register.LastName;
            user.FirstName = register.FirstName;
            user.CreationDate = DateTime.Now;
            user.BirthDate = register.BirthDate;
            user.ClubID = club.ClubID;
            user.RoleID = roleID;
            db.Users.Add(user);
            db.SaveChanges();

            return user;
        }

        private string CreateSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[32];
            rng.GetBytes(buff);

            return Convert.ToBase64String(buff);
        }

        public string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, salt);
            string hashedPwd =
                    FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "sha1");
            return hashedPwd;
        }
    }


}