﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vereinsverwaltung.Models;

namespace Vereinsverwaltung.Services
{
    public class AuthentificationService
    {
        private ClubContext db = new ClubContext();
        private RegisterService reg = new RegisterService();

        public User login(String Email, String Password)
        {

            var userDB = from u in db.Users where u.Email == Email select u;
            if (userDB.Count() == 0) return null;
            User user = userDB.First();
            String passwordHashed = reg.CreatePasswordHash(Password, user.PasswordSalt);

            //For testing without encryption
            if (userDB.Count() > 0 && (user.Password == Password))
            {
                return user;
            }

            return null;
        }

        public int getRolleId(int userId)
        {
            int rolleId = -1;

            var user = this.getUser(userId);

            if (user != null)
            {
                rolleId = user.RoleID;
            }

            return rolleId;
        }

        public int getClubIdByEmail(String email)
        {
            int clubId = -1;

            var user = this.getUserByEmail(email);

            if (user != null)
            {
                clubId = user.ClubID;
            }

            return clubId;
        }

        public int getClubId(int userId)
        {
            int clubId = -1;

            var user = this.getUser(userId);

            if (user != null)
            {
                clubId = user.ClubID;
            }

            return clubId;
        }

        public User getUserByEmail(String email)
        {
            var user = from u in db.Users where u.Email == email select u;

            if (user.Count() > 0) return user.First();

            //else
            return null;
        }

        private User getUser(int userId)
        {
            var user = from u in db.Users where u.UserID == userId select u;

            if (user.Count() > 0) return user.First();

            //else
            return null;
        }

        public Boolean isCashier(int userId)
        {
            User user = this.getUser(userId);
            if (user != null && user.Role.Name == "Kassier")
            {
                return true;
            }
            return false;
        }
    }
}