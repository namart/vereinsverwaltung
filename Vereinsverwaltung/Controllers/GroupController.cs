﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vereinsverwaltung.Models;
using Vereinsverwaltung.Repositories;
using Vereinsverwaltung.Services;
using System.Diagnostics;
using Vereinsverwaltung.ActionFilter;

namespace Vereinsverwaltung.Controllers
{
    [GlobalAuthorizeAttribute]
    public class GroupController : Controller
    {
        private ClubContext db = new ClubContext();
        private AuthentificationService auth = new AuthentificationService();

        private IUserRepository userRepository = new UserRepository();
        private IGroupRepository groupRepository = new GroupRepository();

        //
        // GET: /Group/

        [RoleAuthorizeAttribute]
        public ActionResult Index()
        {
            int clubID = (int)ViewData["ClubId"];
            return View(this.groupRepository.getClubGroups(clubID));
        }

        //
        // GET: /Group/Details/5

        [GroupAuthorizeAttribute]
        [RoleAuthorizeAttribute]
        public ActionResult Details(int id)
        {
            Group group = this.groupRepository.GetGroupByID(id);
            return View(group);
        }

        //
        // GET: /Group/Create

        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Group/Create

        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        [HttpPost]
        public ActionResult Create(Group group)
        {
            if (ModelState.IsValid)
            {
                int clubID = (int)ViewData["ClubId"];

                group.ClubID = clubID;
                this.groupRepository.InsertGroup(group);
                this.groupRepository.Save();
                return RedirectToAction("Edit", new { id = group.GroupID });
            }

            return View(group);
        }
        
        //
        // GET: /Group/Edit/5

        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        public ActionResult Edit(int id)
        {

            Group group = this.groupRepository.GetGroupByID(id);
            if (group == null) return RedirectToAction("index");
            IEnumerable<User> clubUsers = this.userRepository.getClubUsers(group.ClubID);
            List<User> availableGroupUsers = new List<User>();

            foreach (var clubUser in clubUsers)
            {
                Boolean isInside = false;
                foreach (var groupUser in group.Users)
                {
                    if (clubUser.UserID == groupUser.UserID)
                    {
                        isInside = true;
                    }
                }
                if (!isInside)
                {
                    availableGroupUsers.Add(clubUser);
                }
            }
            ViewBag.ClubUsers = availableGroupUsers;
            return View(group);

        }

        //
        // POST: /Group/Edit/5

        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        [HttpPost]
        public ActionResult Edit(Group group)
        {
            if (ModelState.IsValid)
            {
                this.groupRepository.UpdateGroup(group);
                this.groupRepository.Save();
                return RedirectToAction("Index");

            }
            return View(group);
        }

        //
        // GET: /Group/Delete/5

        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        public ActionResult Delete(int id)
        {
            Group group = this.groupRepository.GetGroupByID(id);
            return View(group);
        }

        //
        // POST: /Group/Delete/5

        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            this.groupRepository.DeleteGroup(id);
            this.groupRepository.Save();

            return RedirectToAction("Index");
        }

        //
        // GET: /Group/5/AddUser/1

        public ActionResult AddUser(int groupid, int userid)
        {
            this.groupRepository.InsertUser(groupid, userid);
            this.groupRepository.Save();
            return Json("saved", JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Group/5/RemoveUser/1

        public ActionResult RemoveUser(int groupid, int userid)
        {
            this.groupRepository.RemoveLeader(groupid, userid);
            this.groupRepository.Save();
            this.groupRepository.RemoveUser(groupid, userid);
            this.groupRepository.Save();
            return Json("saved", JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Group/5/AddLeader/1

        public ActionResult AddLeader(int groupid, int userid)
        {
            this.groupRepository.AddLeader(groupid, userid);
            this.groupRepository.Save();
            return Json("saved", JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Group/5/RemoveLeader/1

        public ActionResult RemoveLeader(int groupid, int userid)
        {
            this.groupRepository.RemoveLeader(groupid, userid);
            this.groupRepository.Save();
            return Json("saved", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public int getClubID()
        {
            if (Session["clubID"] != null)
            {
                return (int)Session["clubID"];
            }
            else
            {
                int clubID = auth.getClubIdByEmail(System.Web.HttpContext.Current.User.Identity.Name);
                Session.Add("clubID", clubID);
                return clubID;
            }
        }
    }
}