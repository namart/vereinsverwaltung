﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vereinsverwaltung.Models;
using Vereinsverwaltung.Services;
using Vereinsverwaltung.Repositories;
using System.Web.Security;
using Vereinsverwaltung.ActionFilter;
using Vereinsverwaltung.Models.ViewModels;

namespace Vereinsverwaltung.Controllers
{
    [GlobalAuthorizeAttribute]
    public class UserController : Controller
    {

        private AuthentificationService auth = new AuthentificationService();
        private RegisterService reg = new RegisterService();
        private ClubContext db = new ClubContext();

        private IUserRepository userRepository = new UserRepository();
        private IGroupRepository groupRepository = new GroupRepository();


        //
        // GET: /User/

        [RoleAuthorizeAttribute]
        public ActionResult Index()
        {
            int clubID = (int)ViewData["ClubId"];
            ViewBag.Groups = this.groupRepository.getClubGroups(clubID);
            return View(this.userRepository.getClubUsers(clubID));
        }

        //
        // GET: /User/Search

        [RoleAuthorizeAttribute]
        public ActionResult Search()
        {
            int clubID = (int)ViewData["ClubId"];
            ViewBag.Groups = this.groupRepository.getClubGroups(clubID);
            return View(this.userRepository.getClubUsers(clubID));
        }

        //
        // GET: /User/Details/5

        [RoleAuthorizeAttribute]
        [UserAuthorizeAttribute]
        public ActionResult Details(int id)
        {
            User user = this.userRepository.GetUsertByID(id);
            return View(user);
        }

        //
        // GET: /User/Create

        [RoleAuthorizeAttribute(Role = new string[] { "Leiter", "Gruppenleiter" })]
        public ActionResult Create()
        {
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "Name");

            List<Group> Groups =  this.groupRepository.getClubGroups((int)ViewData["ClubId"]).ToList();

            /*
            if (ViewData["Role"] != null && ViewData["Role"] == "Gruppenleiter")
            {
                // Nicht Gruppenleiter aus Groups entfernen
            }
            */

            ViewBag.GroupID = new SelectList(Groups, "GroupID", "Name");
            return View();            
        } 

        //
        // POST: /User/Create

        [HttpPost]
        [RoleAuthorizeAttribute(Role = new string[] { "Leiter", "Gruppenleiter" })]
        public ActionResult Create(UserViewModel userViewModel)
        {
            if (ModelState.IsValid)
            {
                User user = new User();
                user.FirstName = userViewModel.FirstName;
                user.LastName = userViewModel.LastName;
                user.Password = userViewModel.Password;
                user.Email = userViewModel.Email;
                user.BirthDate = userViewModel.BirthDate;
                user.RoleID = userViewModel.RoleID;

                int clubID = (int)ViewData["ClubId"];
                user.CreationDate = DateTime.Now;
                user.ClubID = clubID;
                user = reg.CreateUser(user);            

                // Check ob Benutzer user in Gruppe geben darf, fehlt!
                if (userViewModel.GroupID != 0)
                {
                    this.groupRepository.InsertUser(userViewModel.GroupID, user.UserID);
                    this.groupRepository.Save();
                }

                return RedirectToAction("Index");
            }

            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "Name", userViewModel.RoleID);
            ViewBag.GroupID = new SelectList(groupRepository.getClubGroups((int)ViewData["ClubId"]), "GroupID", "Name");
            return View(userViewModel);

        }
        
        //
        // GET: /User/Edit/5

        [UserAuthorizeAttribute]
        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        public ActionResult Edit(int id)
        {
            if (id == null) return RedirectToAction("Index");
            User user = this.userRepository.GetUsertByID(id);
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "Name", user.RoleID);
            ViewBag.ClubID = new SelectList(db.Clubs, "ClubID", "Name", user.ClubID);
            return View(user);
            
        }

        //
        // POST: /User/Edit/5
    
        [HttpPost]
        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        public ActionResult Edit(User user, int id)
        {
            if (ModelState.IsValid)
            {
                this.userRepository.UpdateUser(user);
                this.userRepository.Save();
                return RedirectToAction("Index");

            }
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "Name", user.RoleID);
            ViewBag.ClubID = new SelectList(db.Clubs, "ClubID", "Name", user.ClubID);
            return View(user);

            
        }

        //
        // GET: /User/Delete/5

        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        public ActionResult Delete(int id)
        {

            User user = this.userRepository.GetUsertByID(id);
            return View(user);

            
        }

        //
        // POST: /User/Delete/5

        [RoleAuthorizeAttribute(Role = new string[] { "Leiter" })]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {

            this.userRepository.DeleteUser(id);
            this.userRepository.Save();

            return RedirectToAction("Index");
            
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}