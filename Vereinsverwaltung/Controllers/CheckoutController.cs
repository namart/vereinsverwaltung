﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vereinsverwaltung.Models;
using Vereinsverwaltung.Repositories;
using Vereinsverwaltung.Services;
using System.Collections;
using Vereinsverwaltung.ActionFilter;

namespace Vereinsverwaltung.Controllers
{
    [GlobalAuthorizeAttribute]
    [RoleAuthorizeAttribute(Role = new string[] { "Leiter", "Kassier" })]
    public class CheckoutController : Controller
    {
        private ClubContext db = new ClubContext();
        private AuthentificationService auth = new AuthentificationService();

        private IUserRepository userRepository = new UserRepository();
        private IGroupRepository groupRepository = new GroupRepository();
        private ICheckoutRepository checkoutRepository = new CheckoutRepository();
        private ITransactionRepository transactionRepository = new TransactionRepository();

        //
        // GET: /Checkout/

        public ActionResult Index()
        {

            int clubID = (int)ViewData["ClubId"];
            return View(this.checkoutRepository.getClubCheckouts(clubID));

        }

        //
        // GET: /Checkout/Create

        public ActionResult Create()
        {
            List<Group> Groups = this.groupRepository.getClubGroups((int)ViewData["ClubId"]).ToList();
            ViewBag.GroupID = new SelectList(Groups, "GroupID", "Name");
            return View();   
        } 

        //
        // POST: /Checkout/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Checkout/Edit/5
 
        public ActionResult Edit(int id)
        {
            /*
        Checkout checkout = this.checkoutRepository.GetCheckoutByID(id);
        Hashtable clubUsersOpen = this.transactionRepository.GetOpenUser(id);
        ViewBag.clubUsersOpen = clubUsersOpen;
          
        foreach (var g in clubUsersOpen.Keys)
        {
            foreach (var u in (ArrayList)clubUsersOpen[g])
            {
                Boolean isInside = false;
                foreach (var trans in checkout.Transactions)
                {
                    if (clubUser.UserID == trans.User.UserID)
                    {
                        isInside = true;
                    }
                }
                if (!isInside)
                {
                    //availableGroupUsers.Add(clubUser);
                }
            }
        }*/
            //ViewBag.ClubUsers = availableGroupUsers;
            return View();

        }

        //
        // POST: /Checkout/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Checkout/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Checkout/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
