﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vereinsverwaltung.Models.ViewModels;
using Vereinsverwaltung.Models;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Diagnostics;


namespace Vereinsverwaltung.Controllers
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class SearchUserController : Controller
    {

        private ClubContext context = new ClubContext();
        private List<User> results;

        [WebMethod]
        public JsonResult Index(SearchModel searchModel)
        {
            List<Object> ret = new List<Object>();
            if ((searchModel.FirstName != null && !searchModel.FirstName.Equals(String.Empty) )  || (searchModel.LastName != null && !searchModel.LastName.Equals(String.Empty)))
            {
                this.results = context.Users.Where(x => x.FirstName.Contains(searchModel.FirstName) || x.LastName.Contains(searchModel.LastName)).ToList();
            }
            else if ((searchModel.Text != null) && (!searchModel.Text.Equals(String.Empty)))
            {
                this.results = context.Users.Where(x => x.FirstName.Contains(searchModel.Text) || x.LastName.Contains(searchModel.Text) || x.Email.Contains(searchModel.Text)).ToList();
            }
            else
            {
                this.results = context.Users.Where(x => x.ClubID.Equals(searchModel.ClubId)).ToList();
            }
         
            List<User> usersToRemove = new List<User>();

            //remove user not in group
            if (searchModel.Group > 0)
            {
                foreach (User user in this.results)
                {    
                    if (user.Groups.Count > 0)
                    {
                        foreach (Group group in user.Groups)
                        {
                            if (group.GroupID != searchModel.Group)
                            {
                                usersToRemove.Add(user);
                            }
                        }
                    }
                    else
                    {
                        usersToRemove.Add(user);
                    }
                    
                }
            }

            //remove false users
            for (int i = 0; i < usersToRemove.Count; i++)
            {
                results.Remove(usersToRemove.ElementAt(i));
            }

            foreach (User user in this.results)
            {

                ret.Add(new 
                {
                    id = user.UserID,
                    firstname = user.FirstName,
                    lastname = user.LastName,
                    email = user.Email,
                    birthdate = user.BirthDate.ToString("dd/MM/yyyy"),
                    role = user.Role.Name
                });

            }
            return Json(ret, JsonRequestBehavior.AllowGet);
        }


    }
}
