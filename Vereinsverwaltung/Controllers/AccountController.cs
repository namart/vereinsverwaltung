﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vereinsverwaltung.Models;
using Vereinsverwaltung.Services;
using System.Web.Security;
using Vereinsverwaltung.ActionFilter;

namespace Vereinsverwaltung.Controllers
{
    [GlobalAuthorizeAttribute]
    public class AccountController : Controller
    {

        private AuthentificationService auth = new AuthentificationService();

        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(Login login)
        {
            if (ModelState.IsValid)
            {

                User user = auth.login(login.Email, login.Password);

                if (user != null)
                {
                    //Session.Add("clubID", user.ClubID);
                    //Session.Add("userID", user.UserID);
                    FormsAuthentication.SetAuthCookie(login.Email, false);
                    return RedirectToAction("Search", "User");
                }
                else
                {
                    ModelState.AddModelError("Email", "Email not found");

                }
            }

            return View(login);
        }

        public ActionResult Logout()
        {
            //Session.Remove("clubID");
            //Session.Remove("userID");
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult AccessViolation()
        {
            return View();
        }

    }
}
