﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vereinsverwaltung.Models;
using Vereinsverwaltung.Services;
using System.Web.Security;
using Vereinsverwaltung.Repositories;
using Vereinsverwaltung.ActionFilter;

namespace Vereinsverwaltung.Controllers
{
    [GlobalAuthorizeAttribute]
    public class ClubController : Controller
    {
        private ClubContext db = new ClubContext();

        private AuthentificationService auth = new AuthentificationService();
        private RegisterService reg = new RegisterService();
        private IClubRepository clubRepository = new ClubRepository();
        private IUserRepository userRepository = new UserRepository();


        [RoleAuthorizeAttribute(Redirect = "Club/Home")]
        public ActionResult Index()
        {
            return View(this.clubRepository.GetClubByID((int)ViewData["ClubId"]));
        }

        public ActionResult Home()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("index");
            }
            else
            {
                ViewBag.Message = "Willkommen Auf der Vereinsverwaltung App";

                return View();
            }
            
        }

        //
        // GET: /Club/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Club/Create

        [HttpPost]
        public ActionResult Create(Register register)
        {
            if (ModelState.IsValid)
            {
                User user = reg.CreateClubUser(register, 1);
                //Session.Add("clubID", user.ClubID);
                FormsAuthentication.SetAuthCookie(register.Email, false);
                return RedirectToAction("Index", "User");  
            }

            return View();
        }
        
        //
        // GET: /Club/Edit/

        [RoleAuthorizeAttribute(Redirect = "Club/Home", Role = new string[] { "Leiter" })]
        public ActionResult Edit()
        {
            Club club = this.clubRepository.GetClubByID((int)ViewData["ClubId"]);
            return View(club);

        }

        //
        // POST: /Club/Edit/

        [HttpPost]
        [RoleAuthorizeAttribute(Redirect = "Club/Home", Role = new string[] { "Leiter" })]
        [ClubAuthorizeAttribute]
        public ActionResult Edit(Club club)
        {

            if (ModelState.IsValid)
            {
                this.clubRepository.UpdateClub(club);
                this.clubRepository.Save();
                return RedirectToAction("Index");
            }
            return View(club);

        }
    }
}