﻿$(function () {

    ///Bootstrap functions
    $('#topbar').dropdown();
    $(".alert-message").alert();
    $('.tabs').tabs();

    // DatePicker
    $("#BirthDate, #MaturityDate").datepicker();

    //Sortable Widget
    $("#users-sortable1, #users-sortable2").sortable({
        connectWith: ".group-user-list",
        placeholder: "ui-state-highlight"
    }).disableSelection();
    //selectable
    $("#users-sortable2 li").live("click", function () {
        if ($(this).hasClass("selected")) {
            
            removeGroupLeader($(this));
        } else {
            $(this).addClass("selected");
            addGroupLeader($(this));
        }
    });

    $("#users-sortable2").bind("sortreceive", addGroupMembers);
    $("#users-sortable2").bind("sortremove", removeGroupMembers);


    //Visual Search
    var visualSearch = VS.init({
        container: $('.visual_search'),
        query: '',
        callbacks: {
            search: search,
            facetMatches: function (callback) {
                callback([
                    'Gruppe', 'Zahlungen', 'Vorname', 'Nachname', ])
            },
            valueMatches: function (facet, searchTerm, callback) {
                switch (facet) {
                    case 'Gruppe':
                        callback(GROUPS);
                        break;
                    case 'Zahlungen':
                        callback(['offen', 'keine']);
                        break;
                }
            }
        }
    });

});

function search(query, searchCollection) {

    var facets = searchCollection.models || [];
    var searchObj = {
        ClubId: CLUBID,
        Group: "",
        Payment: "",
        FirstName: "",
        LastName: "",
        Text: "" 
    }

    for (var i = 0, len = facets.length; i < len; ++i) {
        var facet = facets[i];
        switch (facet.attributes.category) {
            case 'Gruppe':
                searchObj.Group = facet.attributes.value;
                break;
            case 'Zahlungen':
                searchObj.Payment = facet.attributes.value;
                break;
            case 'Vorname':
                searchObj.FirstName = facet.attributes.value;
                break;
            case 'Nachname':
                searchObj.LastName = facet.attributes.value;
                break;
            case 'text':
                searchObj.Text = facet.attributes.value;
                break;
        }
    }


    $.ajax({
        url: "/searchuser",
        contentType: "application/json",
        dataType: "json",
        type: "POST",
        processData: false,
        data: JSON.stringify(searchObj),
        success: function (data) {
            $('#search-result').html("");
            for (var i = 0, len = data.length; i < len; ++i) {
                var user = ich.usertemplate(data[i]);
                $('#search-result').append(user);
            }
            var url = '?';
            for (var facet in searchObj) {
                url += facet + '=' + searchObj[facet] + '&';
            }
            url = url.slice(0, url.length - 1);
            $.address.value(url);
        }
    });

}

function addGroupMembers(event, ui) {
    $.ajax({
        url: GROUPID + "/AddUser/" + ui.item.data("user-id"),
        success: function (data) {
            console.log(data);
        }
    });

}

function removeGroupMembers(event, ui) {
    $.ajax({
        url: GROUPID + "/RemoveUser/" + ui.item.data("user-id"),
        success: function (data) {
            console.log(data);
        }
    });
    //removeGroupLeader(ui.item);
    ui.item.removeClass("selected");
}

function addGroupLeader(el) {
    $.ajax({
        url: GROUPID + "/AddLeader/" + el.data("user-id"),
        success: function (data) {
            el.addClass("selected");
        }
    });
}

function removeGroupLeader(el) {
    $.ajax({
        url: GROUPID + "/RemoveLeader/" + el.data("user-id"),
        success: function (data) {
            el.removeClass("selected");
        }
    });
}