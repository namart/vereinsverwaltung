﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Vereinsverwaltung.Models
{
    public class ClubContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupLeaderAssignment> GroupLeaderAssignments { get; set; }
        public DbSet<Checkout> Checkouts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}
