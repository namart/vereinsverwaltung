﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vereinsverwaltung.Models;
using System.Data.Entity;

namespace Vereinsverwaltung.DAL
{
    public class ClubInitializer : DropCreateDatabaseAlways<ClubContext>
    {
        protected override void Seed(ClubContext context)
        {
            var roles = new List<Role>
            {
                new Role { Name = "Leiter" },
                new Role { Name = "Kassier" },
                new Role { Name = "Mitglied" },
            };
            roles.ForEach(s => context.Roles.Add(s));
            context.SaveChanges();

            var clubs = new List<Club>
            {
                new Club { Name = "Schwimmverein",  CreationDate = DateTime.Parse("2005-09-01") },
                new Club { Name = "KunstKlub", CreationDate = DateTime.Parse("2005-09-01") } 
            };
            clubs.ForEach(s => context.Clubs.Add(s));
            context.SaveChanges();

            var users = new List<User>
            {
                new User { FirstName = "Carson",   LastName = "Alexander", Email = "test@musterseite.at", Password = "xxx", CreationDate = DateTime.Parse("2005-09-01"), BirthDate = DateTime.Parse("1975-07-03"), RoleID = 1, ClubID = 1},
                new User { FirstName = "Kumbeiz",   LastName = "Alexander", Email = "abd@musterseite.at", Password = "xxx", CreationDate = DateTime.Parse("2005-09-01"), BirthDate = DateTime.Parse("1975-09-01"), RoleID = 2, ClubID = 1},
                new User { FirstName = "Hobl",   LastName = "Samuel", Email = "dfg@musterseite.at", Password = "xxx", CreationDate = DateTime.Parse("2005-09-01"), BirthDate = DateTime.Parse("1935-02-06"), RoleID = 3, ClubID = 1},
                new User { FirstName = "Samuel",   LastName = "Alexander", Email = "hgf@musterseite.at", Password = "xxx", CreationDate = DateTime.Parse("2005-09-01"), BirthDate = DateTime.Parse("1975-05-01"), RoleID = 3, ClubID = 1},
                new User { FirstName = "2Hobl",   LastName = "Samuel", Email = "swrzh@musterseite.at", Password = "xxx", CreationDate = DateTime.Parse("2005-09-01"), BirthDate = DateTime.Parse("1975-04-01"), RoleID = 1, ClubID = 2},
                new User { FirstName = "3Kumbeiz",   LastName = "Alexander", Email = "ktzu@musterseite.at", Password = "xxx", CreationDate = DateTime.Parse("2005-09-01"), BirthDate = DateTime.Parse("1994-09-05"), RoleID = 3, ClubID = 2},
                new User { FirstName = "3Hobl",   LastName = "Samuel", Email = "wz@musterseite.at", Password = "xxx", CreationDate = DateTime.Parse("2005-09-01"), BirthDate = DateTime.Parse("1985-09-01"), RoleID =3, ClubID = 2},
            };
            users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();

       
            var groups = new List<Group>
            {
                new Group { Name = "Schwimmverein",  Description = "kleine beschreibung1", ClubID = 1, Users = new List<User>()},
                new Group { Name = "KunstKlub", Description = "kleine beschreibung2", ClubID = 2, Users = new List<User>()} 
            };
            groups.ForEach(s => context.Groups.Add(s));
            context.SaveChanges();

            groups[0].Users.Add(users[0]);
            groups[0].Users.Add(users[1]);
            groups[1].Users.Add(users[4]);
            context.SaveChanges();

           var groupLeaderAssignments = new List<GroupLeaderAssignment>
           {
               new GroupLeaderAssignment { GroupID = 1, UserID = 2 },
               new GroupLeaderAssignment { GroupID = 2, UserID = 5 } 
           };
           groupLeaderAssignments.ForEach(s => context.GroupLeaderAssignments.Add(s));
           context.SaveChanges();

           var checkouts = new List<Checkout>
            {
                new Checkout { ClubID = clubs[0].ClubID, Group = groups[0], Amount = 200, MaturityDate = DateTime.Parse("2012-01-01"), CheckoutID = 1, Transactions = new List<Transaction>() },
                new Checkout { ClubID = clubs[0].ClubID, Group = groups[0], Amount = 78, MaturityDate = DateTime.Parse("2012-01-09"), CheckoutID = 2, Transactions = new List<Transaction>() },
                new Checkout { ClubID = clubs[0].ClubID, Group = groups[1], Amount = 55, MaturityDate = DateTime.Parse("2012-05-01"), CheckoutID = 3, Transactions = new List<Transaction>() },
                new Checkout { ClubID = clubs[0].ClubID, Group = groups[1], Amount = 19, MaturityDate = DateTime.Parse("2012-01-10"), CheckoutID = 4, Transactions = new List<Transaction>() }
            };
           checkouts.ForEach(s => context.Checkouts.Add(s));
           context.SaveChanges();

           var transactions = new List<Transaction>
            {
                new Transaction { TransactionID = 1, User = users[0], Checkout = checkouts[0], Amount = 500, PayDate = DateTime.Parse("2011-12-10") },
                new Transaction { TransactionID = 2, User = users[1], Checkout = checkouts[0], Amount = 77, PayDate = DateTime.Parse("2011-12-10") }
            };
           transactions.ForEach(s => context.Transactions.Add(s));
           context.SaveChanges();

        }
    }
}