﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vereinsverwaltung.Models;
using System.Linq.Expressions;

namespace Vereinsverwaltung.Repositories
{
    interface IUserRepository
    {
        IEnumerable<User> getClubUsers(int clubID);
        User GetUsertByID(int usertId);
        User GetUsertByEmail(String email);
        void InsertUser(User user);
        void DeleteUser(int usertId);
        void UpdateUser(User user);
        void Save();

    }
}
