﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vereinsverwaltung.Models;
using System.Diagnostics;

namespace Vereinsverwaltung.Repositories
{
    public class GroupRepository : IGroupRepository
    {
        private ClubContext context = new ClubContext();

        public IEnumerable<Models.Group> getClubGroups(int clubID)
        {
            return context.Groups.Where(i => i.ClubID == clubID).ToList();
        }

        public Group GetGroupByID(int id)
        {
            return context.Groups.Find(id);
        }

        public void InsertGroup(Group group)
        {
            context.Groups.Add(group);
        }

        public void InsertUser(int groupid, int userid)
        {
            Group group = this.GetGroupByID(groupid);
            User user = context.Users.Find(userid);
            group.Users.Add(user);
        }

        public void RemoveUser(int groupid, int userid)
        {
            Group group = this.GetGroupByID(groupid);
            User user = context.Users.Find(userid);
            group.Users.Remove(user);
        }

        public void DeleteGroup(int id)
        {
            Group group = context.Groups.Find(id);
            context.Groups.Remove(group);
        }

        public void UpdateGroup(Group group)
        {
            var groupInDb = this.GetGroupByID(group.GroupID);
            group.ClubID = groupInDb.ClubID;
            context.Entry(groupInDb).CurrentValues.SetValues(group);
        }

        public void AddLeader(int groupid, int userid)
        {
            Group group = this.GetGroupByID(groupid);
            List<GroupLeaderAssignment> assigment = context.GroupLeaderAssignments.Where(x => x.GroupID.Equals(groupid) && x.UserID.Equals(userid)).ToList();
            if (assigment.Count() > 0)
            {
                return;
            }
            GroupLeaderAssignment assignment = new GroupLeaderAssignment();
            assignment.UserID = userid;
            assignment.GroupID = groupid;
            group.GroupLeaderAssignments.Add(assignment);
        }

        public void RemoveLeader(int groupid, int userid)
        {
            Group group = this.GetGroupByID(groupid);
            List<GroupLeaderAssignment> assigment = context.GroupLeaderAssignments.Where(x => x.GroupID.Equals(groupid) && x.UserID.Equals(userid)).ToList();
            if (assigment.Count() > 0)
            {
                context.GroupLeaderAssignments.Remove(assigment.First());
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}