﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vereinsverwaltung.Models;
using System.Collections;

namespace Vereinsverwaltung.Repositories
{
    interface ICheckoutRepository
    {
        IEnumerable<Checkout> getClubCheckouts(int clubID);
        Checkout GetCheckoutByID(int id);
        void InsertCheckout(Checkout checkout);
        void DeleteCheckout(int id);
        void UpdateCheckout(Checkout checkout);
        void Save();
    }
}
