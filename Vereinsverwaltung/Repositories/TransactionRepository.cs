﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vereinsverwaltung.Models;
using System.Diagnostics;
using System.Collections;

namespace Vereinsverwaltung.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private ClubContext context = new ClubContext();

        public IEnumerable<Models.Transaction> getClubTransactions()
        {
            return context.Transactions.ToList();
        }

        public Transaction GetTransactionByID(int id)
        {
            return context.Transactions.Find(id);
        }

        public Hashtable GetOpenUser(int clubID)
        {
            Hashtable payedUser = new Hashtable();
            
            Hashtable allUser = new Hashtable();
            // Gruppen in Club
            foreach (var g in context.Groups.Where(g => g.ClubID == clubID).ToList()) {
                // CHeckouts in Gruppe
                ArrayList all = new ArrayList();
                foreach (var c in g.Checkouts.ToList()) {
                    // User in Gruppe
                    foreach (var u in g.Users.ToList()) {
                        
                        ArrayList payed = new ArrayList();
                        foreach (var t in c.Transactions.ToList()) {
                            if(t.User.UserID == u.UserID) {
                                payed.Add(u);
                            }
                        }
                        if (payed.Count > 0)
                            payedUser.Add(g, payed);
                    }
                    
                }
                allUser.Add(g, all);
            }

            foreach( var p in payedUser.Keys) {
                foreach( var u in (ArrayList)payedUser[p]) {
                    foreach (var pAll in allUser.Keys) {
                        foreach (var uAll in (ArrayList)allUser[pAll]) {
                            if(((Group)p).GroupID == ((Group)pAll).GroupID && ((User)u).UserID == ((User)uAll).UserID) {
                                ((ArrayList)allUser[pAll]).RemoveAt(((ArrayList)allUser[pAll]).IndexOf(uAll));
                            }
                        }
                    }
                }
            }
            return allUser;
        }

        public void InsertTransaction(Transaction transaction)
        {
            context.Transactions.Add(transaction);
        }

        public void DeleteTransaction(int id)
        {
            Transaction transaction = context.Transactions.Find(id);
            context.Transactions.Remove(transaction);
        }

        public void UpdateTransaction(Transaction transaction)
        {
            var transactionInDb = this.GetTransactionByID(transaction.TransactionID);
            //transaction.ClubID = transactionInDb.ClubID;
            context.Entry(transactionInDb).CurrentValues.SetValues(transaction);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}