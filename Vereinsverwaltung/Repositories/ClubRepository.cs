﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vereinsverwaltung.Models;
using System.Data;

namespace Vereinsverwaltung.Repositories
{
    public class ClubRepository : IClubRepository
    {
        private ClubContext context = new ClubContext();

        public IEnumerable<Club> getClubs()
        {
            return context.Clubs.ToList();
        }

        public Club GetClubByID(int id)
        {
            return context.Clubs.Find(id);
        }

        public void InsertClub(Club club)
        {
            context.Clubs.Add(club);
        }

        public void DeleteClub(int id)
        {
            Club club = context.Clubs.Find(id);
            context.Clubs.Remove(club);

        }

        public void UpdateClub(Club club)
        {
            var clubInDb = this.GetClubByID(club.ClubID);
            club.CreationDate = clubInDb.CreationDate;
            context.Entry(clubInDb).CurrentValues.SetValues(club);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}