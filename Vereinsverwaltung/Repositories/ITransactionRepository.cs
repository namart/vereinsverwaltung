﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vereinsverwaltung.Models;
using System.Collections;

namespace Vereinsverwaltung.Repositories
{
    interface ITransactionRepository
    {
        IEnumerable<Transaction> getClubTransactions();
        Transaction GetTransactionByID(int id);
        Hashtable GetOpenUser(int clubID);
        void InsertTransaction(Transaction transaction);
        void DeleteTransaction(int id);
        void UpdateTransaction(Transaction transaction);
        void Save();
    }
}
