﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vereinsverwaltung.Models;

namespace Vereinsverwaltung.Repositories
{
    interface IClubRepository
    {    
        IEnumerable<Club> getClubs();
        Club GetClubByID(int id);
        void InsertClub(Club club);
        void DeleteClub(int id);
        void UpdateClub(Club club);
        void Save();
    }
}
