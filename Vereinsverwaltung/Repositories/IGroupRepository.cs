﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vereinsverwaltung.Models;

namespace Vereinsverwaltung.Repositories
{
    interface IGroupRepository
    {
        IEnumerable<Group> getClubGroups(int clubID);
        Group GetGroupByID(int id);
        void InsertGroup(Group group);
        void DeleteGroup(int id);
        void UpdateGroup(Group group);
        void InsertUser(int groupid, int userid);
        void RemoveUser(int groupid, int userid);
        void AddLeader(int groupid, int userid);
        void RemoveLeader(int groupid, int userid);
        void Save();
    }
}
