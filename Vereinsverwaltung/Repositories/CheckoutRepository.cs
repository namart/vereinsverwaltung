﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vereinsverwaltung.Models;
using System.Diagnostics;
using System.Collections;

namespace Vereinsverwaltung.Repositories
{
    public class CheckoutRepository : ICheckoutRepository
    {
        private ClubContext context = new ClubContext();

        public IEnumerable<Models.Checkout> getClubCheckouts(int clubID)
        {
            return context.Checkouts.Where(i => i.ClubID == clubID).ToList();
        }

        public Checkout GetCheckoutByID(int id)
        {
            return context.Checkouts.Find(id);
        }

        public IEnumerable<Models.Transaction> GetOpenCheckouts(int clubID, int test)
        {
            //return context.Transactions.Where(c => c.Checkout.ClubID == clubID).Sum(c => c.Amount).GroupBy(c => c.User.UserID);
            return null;
        }

        public void InsertCheckout(Checkout checkout)
        {
            context.Checkouts.Add(checkout);
        }

        public void DeleteCheckout(int id)
        {
            Checkout checkout = context.Checkouts.Find(id);
            context.Checkouts.Remove(checkout);
        }

        public void UpdateCheckout(Checkout checkout)
        {
            var checkoutInDb = this.GetCheckoutByID(checkout.CheckoutID);
            checkout.ClubID = checkoutInDb.ClubID;
            context.Entry(checkoutInDb).CurrentValues.SetValues(checkout);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}