﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vereinsverwaltung.Models;
using System.Data;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;

namespace Vereinsverwaltung.Repositories
{
    public class UserRepository : IUserRepository
    {
        private ClubContext context = new ClubContext();

        public IEnumerable<User> getClubUsers(int clubID)
        {
            return context.Users.Where(i => i.ClubID == clubID).ToList();
        }

        public User GetUsertByID(int id)
        {
            return context.Users.Find(id);
        }

        public User GetUsertByEmail(string email)
        {
            List<User> userDB = context.Users.Where(x => x.Email.Equals(email)).ToList();
            if (userDB.Count() == 0) return null;
            return userDB.First();
        }

        public void InsertUser(User user)
        {
            context.Users.Add(user);

        }

        public void DeleteUser(int id)
        {
            User user = context.Users.Find(id);
            context.Users.Remove(user);

        }

        public void UpdateUser(User user)
        {
            var userInDb = this.GetUsertByID(user.UserID);
            user.Password = userInDb.Password;
            user.PasswordSalt = userInDb.PasswordSalt;
            user.ClubID = userInDb.ClubID;
            user.CreationDate = userInDb.CreationDate;
            context.Entry(userInDb).CurrentValues.SetValues(user);

        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}